<!DOCTYPE HTML>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="assets/style.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<title>Island Media Phase Test</title>
</head>

<body>

	<div class="wrapper">
		<?php 
		echo "<p>Today is <span class=daynow></span></p>";
		$days = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
		echo "<ul class=list_of_days>";
		foreach ($days as $day) {
			echo "<li class=the_day>$day</li>";
		}
		echo "</ul>";
		echo "<p>Selected day is <span class=selected_day>-</span></p>";
		?>
	</div>

	<script type="text/javascript">
		$(function(){
			days 	= ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
			d 		= new Date();
			dayName = days[d.getDay()-1];
			day 	= $('.the_day');

			// add string day of today
			$('.daynow').text(dayName);

			// change style to dayname if today
			day.eq(new Date().getDay()-1).addClass('today');

			// change style if dayname clicked
			day.click(function() {
				day.removeClass('selected');
				$('.selected_day').text($(this).text());
				$(this).addClass('selected');
			});
		});
	</script>

</body>

</html>